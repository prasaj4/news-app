package com.prasetya.newsapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.prasetya.newsapp.adapter.ListNewsAdapter;
import com.prasetya.newsapp.fragment.FilterFragment;
import com.prasetya.newsapp.fragment.SearchFragment;
import com.prasetya.newsapp.model.NewsModel;
import com.prasetya.newsapp.model.NewsResultModel;
import com.prasetya.newsapp.page.DetailActivity;
import com.prasetya.newsapp.page.ProfileActivity;
import com.prasetya.newsapp.util.Constanta;
import com.prasetya.newsapp.util.retrofit.ApiClient;
import com.prasetya.newsapp.util.retrofit.ApiServices;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener,
        SearchFragment.OnSubmitButtonListener,
        FilterFragment.OnSubmitButtonListener {

    public static final String PUBLISHER = "Publisher";
    private RecyclerView newsRecyclerView;

    private ArrayList<NewsResultModel> list = new ArrayList<>();

    private ListNewsAdapter listNewsAdapter;

    private FloatingActionButton fabSetting, fabSearch, fabSort;

    private Animation animOpen, animClose, animClockwise, animNotClockwise;

    private TextView tvSearch, tvSort;

    private int page = 1;
    private int pageSize = 25;
    private int totalItem = 100;

    private String sort = "publishedAt";
    private String fromDate = "2020-03-30";
    private String toDate = "2020-04-04";
    private String language = "id";
    private String searchKey = "a";

    private boolean isLoading = false;
    private boolean isFabOpen = false;

    private ProgressBar progressBar;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private Calendar cal = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        newsRecyclerView = findViewById(R.id.news_recycler_view);
        newsRecyclerView.setHasFixedSize(true);
        progressBar = findViewById(R.id.circular_loading_news);
        fabSetting = findViewById(R.id.fab_setting);
        fabSearch = findViewById(R.id.fab_search);
        fabSort = findViewById(R.id.fab_sort);
        animOpen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_button_open);
        animClose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_button_close);
        animClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_rotate_clockwise);
        animNotClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_rotate_not_clockwise);

        tvSearch = findViewById(R.id.textview_search);
        tvSort = findViewById(R.id.textview_filter);

        fabSearch.setOnClickListener(this::onClick);
        fabSetting.setOnClickListener(this::onClick);
        fabSort.setOnClickListener(this::onClick);

        cal.add(Calendar.DATE, -7);
        fromDate = simpleDateFormat.format(cal.getTime());
        toDate = simpleDateFormat.format(new Date());

        listNewsAdapter = new ListNewsAdapter(list, this, new ListNewsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(NewsResultModel newsResultModel) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra(Constanta.NEWS_DETAIL, newsResultModel);
                intent.putExtra(Constanta.PUBLISHER, newsResultModel.getSource().getName());
                startActivity(intent);
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);

        newsRecyclerView.setLayoutManager(layoutManager);
        newsRecyclerView.setAdapter(listNewsAdapter);

        newsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1)) {
                    ++page;
                    if (!isLoading) {
                        if (listNewsAdapter.getItemCount() < totalItem) {
                            getNewsWithRetrofit();
                        }
                    }
                    isLoading = true;
                }
            }
        });

        getNewsWithRetrofit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu_bar, menu);
        setTitle("Home");
        return true;
    }

    private void getNewsWithRetrofit() {

        progressBar.setVisibility(View.VISIBLE);

        ApiServices apiServices = ApiClient.newInstance(getApplicationContext()).create(ApiServices.class);
        Call<NewsModel> call = apiServices.getListNews(
                Constanta.API_KEY,
                page,
                sort,
                searchKey,
                pageSize,
                language,
                fromDate,
                toDate
        );
        call.enqueue(new Callback<NewsModel>() {
            @Override
            public void onResponse(Call<NewsModel> call, Response<NewsModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    list.addAll(response.body().getArticles());
                    listNewsAdapter.notifyDataSetChanged();
                }
                isLoading = false;
            }

            @Override
            public void onFailure(Call<NewsModel> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Error" + t, Toast.LENGTH_SHORT).show();
                t.printStackTrace();
                isLoading = false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_search:
                onClickFabSearch();
                break;
            case R.id.fab_sort:
                onClickFabFilter();
                break;
            case R.id.fab_setting:
                onClickFabSetting();
                break;
        }
    }

    private void onClickFabSearch() {
        SearchFragment searchFragment = SearchFragment.newInstance(searchKey);
        searchFragment.show(getSupportFragmentManager(), "Search Fragment");
    }

    private void onClickFabFilter() {
        FilterFragment filterFragment = FilterFragment.newInstance(sort, language, fromDate, toDate);
        filterFragment.show(getSupportFragmentManager(), "Filter Fragment");
    }

    private void onClickFabSetting() {
        if (isFabOpen) {
            tvSearch.setVisibility(View.INVISIBLE);
            tvSort.setVisibility(View.INVISIBLE);
            fabSort.startAnimation(animClose);
            fabSearch.startAnimation(animClose);
            fabSetting.startAnimation(animNotClockwise);
            fabSort.setClickable(false);
            fabSearch.setClickable(false);
            fabSetting.setImageResource(R.drawable.setting_icon);
            isFabOpen = false;
        } else {
            tvSearch.setVisibility(View.VISIBLE);
            tvSort.setVisibility(View.VISIBLE);
            fabSearch.startAnimation(animOpen);
            fabSort.startAnimation(animOpen);
            fabSetting.startAnimation(animClockwise);
            fabSearch.setClickable(true);
            fabSort.setClickable(true);
            fabSetting.setImageResource(R.drawable.close_icon);
            isFabOpen = true;
        }
    }

    @Override
    public void onSubmitButton(String searchText) {
        searchKey = searchText;
        list.clear();
        listNewsAdapter.notifyDataSetChanged();
        page = 1;
        getNewsWithRetrofit();
    }

    @Override
    public void onSubmitButtonFilter(String sortBy, String language, String dateFrom, String dateTo) {
        this.sort = sortBy;
        this.language = language;
        this.fromDate = dateFrom;
        this.toDate = dateTo;
        list.clear();
        listNewsAdapter.notifyDataSetChanged();
        page = 1;
        getNewsWithRetrofit();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_profile:
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(intent);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }
}
