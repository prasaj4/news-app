package com.prasetya.newsapp.util.retrofit;


import com.prasetya.newsapp.model.NewsModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiServices {
    @GET("everything")
    Call<NewsModel> getListNews(
            @Query("apiKey") String api_key,
            @Query("page") int page,
            @Query("sortBy") String sort,
            @Query("q") String query,
            @Query("pageSize") int pageSize,
            @Query("language") String language,
            @Query("from") String from,
            @Query("to") String to
    );
}
