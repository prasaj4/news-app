package com.prasetya.newsapp.util;

public class Constanta {
    public static final String BASE_URL = "https://newsapi.org/v2/";
    public static final String API_KEY = "f7fd324800ca4fd99c9317d19e8b8f0d";

    public static final String SORT_BY = "SortBy";
    public static final String LANGUAGE = "Language";
    public static final String DATE_FROM = "DateFrom";
    public static final String DATE_TO = "DateTo";

    public static final String SEARCH = "search";

    public static final String NEWS_DETAIL = "news_detail";
    public static final String PUBLISHER = "Publisher";

    public static final String TITLE = "title";
    public static final String DETAIL = "detail";
    public static final String URL_IMAGE = "urlImage";
    public static final String URL = "url";
    public static final String AUTHOR = "author";
    public static final String DATE = "date";
}
