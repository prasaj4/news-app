package com.prasetya.newsapp.model;

import java.io.Serializable;
import java.util.ArrayList;

public class NewsModel implements Serializable {
    private int totalResults;
    private String status;
    private ArrayList<NewsResultModel> articles;

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<NewsResultModel> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<NewsResultModel> articles) {
        this.articles = articles;
    }
}
