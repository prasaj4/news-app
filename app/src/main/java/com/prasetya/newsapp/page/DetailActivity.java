package com.prasetya.newsapp.page;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.prasetya.newsapp.R;
import com.prasetya.newsapp.model.NewsResultModel;
import com.prasetya.newsapp.util.Constanta;
import com.prasetya.newsapp.util.DateUtils;

public class DetailActivity extends AppCompatActivity {

    private TextView textViewTitle, textViewDate, textViewDetail, textViewSource, textViewAuthor, textViewLink;
    private ImageView imageDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Detail");

        textViewTitle = findViewById(R.id.textview_title_detail);
        textViewDate = findViewById(R.id.textview_date_detail);
        textViewDetail = findViewById(R.id.textview_detail);
        textViewSource = findViewById(R.id.textview_source);
        textViewAuthor = findViewById(R.id.textview_author);
        textViewLink = findViewById(R.id.textview_link);
        imageDetail = findViewById(R.id.image_view_detail);

        NewsResultModel newsResultModel = (NewsResultModel) getIntent().getSerializableExtra(Constanta.NEWS_DETAIL);
        String publisher = getIntent().getStringExtra(Constanta.PUBLISHER);

        String parseDate = newsResultModel.getPublishedAt().replace("T", " ").replace("Z", "");
        String date = DateUtils.formatDate(
                "yyyy-MM-dd HH:mm:ss",
                "dd MMMM yyyy",
                parseDate
        );

        textViewLink.setText(Html.fromHtml(String.format("<a href=\"%s\">Baca Selengkapnya-></a> ", newsResultModel.getUrl())));
        textViewLink.setMovementMethod(LinkMovementMethod.getInstance());
        textViewTitle.setText(newsResultModel.getTitle());
        textViewAuthor.setText("Penulis : " + newsResultModel.getAuthor());
        textViewSource.setText("Sumber : " + publisher);
        textViewDetail.setText(newsResultModel.getDescription());
        textViewDate.setText(date);
        RequestOptions requestOptions = new RequestOptions().placeholder(R.drawable.ic_launcher_background).error(R.drawable.ic_launcher_background);
        Glide.with(this).load(newsResultModel.getUrlToImage()).apply(requestOptions).into(imageDetail);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

}
