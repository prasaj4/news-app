package com.prasetya.newsapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.prasetya.newsapp.R;
import com.prasetya.newsapp.util.Constanta;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends BottomSheetDialogFragment implements View.OnClickListener {


    TextInputEditText textInputSearch;
    Button buttonSearch;
    SearchFragment.OnSubmitButtonListener listener;
    View imgClose;

    public SearchFragment() {
        // Required empty public constructor
    }

    public static SearchFragment newInstance(String search) {
        SearchFragment searchFragment = new SearchFragment();

        Bundle bundle = new Bundle();
        bundle.putString(Constanta.SEARCH, search);

        searchFragment.setArguments(bundle);

        return searchFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        String searchText = getArguments().getString(Constanta.SEARCH, "");
        textInputSearch = view.findViewById(R.id.text_input_search);
        textInputSearch.setText(searchText);
        buttonSearch = view.findViewById(R.id.button_search);
        imgClose = view.findViewById(R.id.search_button_close);
        buttonSearch.setOnClickListener(this::onClick);
        imgClose.setOnClickListener(this::onClick);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_search:
                listener.onSubmitButton(textInputSearch.getText().toString());
                getActivity().getSupportFragmentManager().popBackStack();
                dismiss();
                break;
            case R.id.search_button_close:
                dismiss();
                break;
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof SearchFragment.OnSubmitButtonListener) {
            listener = (SearchFragment.OnSubmitButtonListener) context;
        } else {
            throw new RuntimeException(context.toString() + "Activity try to implement interface onSubmitButtonListener");
        }
    }

    public interface OnSubmitButtonListener {
        void onSubmitButton(String searchText);
    }
}
