package com.prasetya.newsapp.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.prasetya.newsapp.R;
import com.prasetya.newsapp.util.Constanta;
import com.prasetya.newsapp.util.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    Spinner spinnerSortBy, spinnerLanguage;
    LinearLayout linearLayoutDateFrom, linearLayoutDateTo;
    TextView textViewDateFrom, textViewDateTo;
    Button buttonFilter;
    View imageClose;
    FilterFragment.OnSubmitButtonListener listener;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy");
    Calendar calendarFrom = Calendar.getInstance();
    Calendar calendarTo = Calendar.getInstance();
    DatePickerDialog dateFromPickerDialog;
    DatePickerDialog dateToPickerDialog;

    public FilterFragment() {
        // Required empty public constructor
    }

    public static FilterFragment newInstance(String sortBy, String language, String dateFrom, String dateTo) {
        FilterFragment filterFragment = new FilterFragment();

        Bundle bundle = new Bundle();
        bundle.putString(Constanta.SORT_BY, sortBy);
        bundle.putString(Constanta.LANGUAGE, language);
        bundle.putString(Constanta.DATE_FROM, dateFrom);
        bundle.putString(Constanta.DATE_TO, dateTo);

        filterFragment.setArguments(bundle);

        return filterFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filter, container, false);
        spinnerSortBy = view.findViewById(R.id.spinner_sort_by);
        spinnerLanguage = view.findViewById(R.id.spinner_language);
        linearLayoutDateFrom = view.findViewById(R.id.linear_layout_date_from);
        linearLayoutDateTo = view.findViewById(R.id.linear_layout_date_to);
        textViewDateFrom = view.findViewById(R.id.text_date_from);
        textViewDateTo = view.findViewById(R.id.text_date_to);
        buttonFilter = view.findViewById(R.id.button_filter);
        imageClose = view.findViewById(R.id.filter_button_close);

        linearLayoutDateFrom.setOnClickListener(this::onClick);
        linearLayoutDateTo.setOnClickListener(this::onClick);
        buttonFilter.setOnClickListener(this::onClick);
        imageClose.setOnClickListener(this::onClick);

        setSpinnerSelectedValue(
                getArguments().getString(Constanta.SORT_BY, "publishedAt"),
                getArguments().getString(Constanta.LANGUAGE, "id")
        );

        textViewDateFrom.setText(DateUtils.formatDate(
                "yyyy-MM-dd",
                "dd MMMM yyyy",
                getArguments().getString(Constanta.DATE_FROM)
        ));

        textViewDateTo.setText(DateUtils.formatDate(
                "yyyy-MM-dd",
                "dd MMMM yyyy",
                getArguments().getString(Constanta.DATE_TO)
                )
        );

        setDateFromPickerDialogData();
        setDateToPickerDialogData();

        return view;
    }

    private void setDateToPickerDialogData() {
        int year = 2000, month = 0, day = 1;
        calendarTo.setTime(DateUtils.saveDate("dd MMMM yyyy", textViewDateTo.getText().toString()));

        year = calendarTo.get(Calendar.YEAR);
        month = calendarTo.get(Calendar.MONTH);
        day = calendarTo.get(Calendar.DAY_OF_MONTH);

        dateToPickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String date = DateUtils.formatDate(
                        "dd M yyyy",
                        "dd MMMM yyyy",
                        dayOfMonth + " " + (month + 1) + " " + year
                );
                textViewDateTo.setText(date);
            }
        }, year, month, day);
    }

    private void setDateFromPickerDialogData() {
        int year = 2000, month = 0, day = 1;
        calendarFrom.setTime(DateUtils.saveDate("dd MMMM yyyy", textViewDateFrom.getText().toString()));

        year = calendarFrom.get(Calendar.YEAR);
        month = calendarFrom.get(Calendar.MONTH);
        day = calendarFrom.get(Calendar.DAY_OF_MONTH);

        dateFromPickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String date = DateUtils.formatDate(
                        "dd M yyyy",
                        "dd MMMM yyyy",
                        dayOfMonth + " " + (month + 1) + " " + year
                );
                textViewDateFrom.setText(date);
            }
        }, year, month, day);
    }

    private void setSpinnerSelectedValue(String sortBy, String language) {
        String[] listSortByValue = getResources().getStringArray(R.array.list_sort_by_value);
        for (int i = 0; i < listSortByValue.length; i++) {
            if (listSortByValue[i] == sortBy) {
                spinnerSortBy.setSelection(i);
                break;
            }
        }
        String[] listLanguageValue = getResources().getStringArray(R.array.list_language_value);
        for (int i = 0; i < listLanguageValue.length; i++) {
            if (listLanguageValue[i] == language) {
                spinnerLanguage.setSelection(i);
                break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linear_layout_date_from:
                dateFromPickerDialog.show();
                break;
            case R.id.linear_layout_date_to:
                dateToPickerDialog.show();
                break;
            case R.id.button_filter:
                String sortBy = getResources().getStringArray(R.array.list_sort_by_value)[spinnerSortBy.getSelectedItemPosition()];
                String language = getResources().getStringArray(R.array.list_language_value)[spinnerLanguage.getSelectedItemPosition()];
                String dateFrom = DateUtils.formatDate(
                        "dd MMMM yyyy",
                        "yyyy-MM-dd",
                        textViewDateFrom.getText().toString());
                String dateTo = DateUtils.formatDate(
                        "dd MMMM yyyy",
                        "yyyy-MM-dd",
                        textViewDateTo.getText().toString()
                );
                listener.onSubmitButtonFilter(sortBy, language, dateFrom, dateTo);
                getActivity().getSupportFragmentManager().popBackStack();
                dismiss();
                break;
            case R.id.filter_button_close:
                dismiss();
                break;
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FilterFragment.OnSubmitButtonListener) {
            listener = (FilterFragment.OnSubmitButtonListener) context;
        } else {
            throw new RuntimeException(context.toString() + "Activity try to implement interface onSubmitButtonListener");
        }
    }

    public interface OnSubmitButtonListener {
        void onSubmitButtonFilter(String sortBy, String language, String dateFrom, String dateTo);
    }

}
