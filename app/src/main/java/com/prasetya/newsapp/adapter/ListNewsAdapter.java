package com.prasetya.newsapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.prasetya.newsapp.R;
import com.prasetya.newsapp.model.NewsResultModel;
import com.prasetya.newsapp.util.DateUtils;

import java.util.ArrayList;

public class ListNewsAdapter extends RecyclerView.Adapter<ListNewsAdapter.ListViewHolder> {
    private ArrayList<NewsResultModel> listNewsModel;
    private Context mContext;
    private OnItemClickListener listener;

    public ListNewsAdapter(ArrayList<NewsResultModel> listNewsModel, Context mContext, OnItemClickListener listener) {
        this.listNewsModel = listNewsModel;
        this.mContext = mContext;
        this.listener = listener;
    }


    @NonNull
    @Override
    public ListNewsAdapter.ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_card, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListNewsAdapter.ListViewHolder holder, int position) {
        NewsResultModel newsResultModel = listNewsModel.get(position);
        RequestOptions requestOptions = new RequestOptions().placeholder(R.drawable.ic_launcher_background).error(R.drawable.ic_launcher_background);
        Glide.with(mContext).load(newsResultModel.getUrlToImage()).apply(requestOptions).into(holder.imgNews);
        holder.tvTitle.setText(newsResultModel.getTitle());
        String parseDate = newsResultModel.getPublishedAt().replace("T", " ").replace("Z", "");
        String date = DateUtils.formatDate(
                "yyyy-MM-dd HH:mm:ss",
                "dd MMMM yyyy",
                parseDate
        );
        holder.tvDate.setText(date);
        holder.cardViewNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(newsResultModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listNewsModel.size();
    }

    public interface OnItemClickListener {
        void onItemClick(NewsResultModel newsResultModel);
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {

        ImageView imgNews;
        TextView tvTitle, tvDate;
        CardView cardViewNews;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            imgNews = itemView.findViewById(R.id.img_view_poster);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvDate = itemView.findViewById(R.id.tv_date);
            cardViewNews = itemView.findViewById(R.id.cardview_news);
        }
    }
}
